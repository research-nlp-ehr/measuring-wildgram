import string
import matplotlib.pyplot as plt
import os
import time
import numpy as np
import shutil
from stanza.server import CoreNLPClient
import regex as re
import string
import os
from wildgram import wildgram
from nltk.corpus import stopwords
import json
import scipy.stats as sp

FOLDERS = ["ngram1", "ngram2", "ngram3", "wildgram","wildgram1gram","stanza","ngram[1, 2, 3]"]


### Turns n2c2 dataset into comparable string indexes ###
def getLineWord(index):
    lineNumber= int(index.split(":")[0])
    wordNumber = int(index.split(":")[1])
    return lineNumber,wordNumber

def findIndex(text, start, end):
    lineNumberS, wordNumberS = getLineWord(start)
    lineNumberE, wordNumberE = getLineWord(end)
    lines = text.split("\n")
    prev = len("\n".join(lines[:lineNumberS-1])) + 1
    prevWord = len(" ".join(lines[lineNumberS-1].split(" ")[0:wordNumberS]))
    if prevWord != 0:
        prevWord = prevWord+1
    start = prev + prevWord
    after = len("\n".join(lines[:lineNumberE-1])) + 1
    afterWord = len(" ".join(lines[lineNumberE-1].split(" ")[0:wordNumberE+1]))
    return start,after+afterWord


def parsei2b2Annotations(file, text, name, annotations):
    for annotation in annotations:
        index = re.findall("\d+\:\d+", annotation)
        if len(index) != 2:
            continue
        start, end = findIndex(text, index[0], index[1])

        line = str(start)+"||"+str(end)+"||"+name+"\n"
        file.write(line)

## creates a folder that has the ast annotations and maps them into a file per record, where it says
## start||end||ast (index of each)
## start||end||none
def parseData(folderpath):
    if not os.path.exists('correct'):
        os.makedirs('correct')
    files = os.listdir(folderpath+"/txt/")
    for file in files:
        if file == ".DS_Store":
            continue
        first = file.split(".")[0]
        text = open(folderpath+"/txt/"+file).read()
        ast = open(folderpath+"/ast/"+first+".ast").read().split("\n")
        try:
            conc = open(folderpath+"/concept/"+first+".con").read().split("\n")
        except:
            conc = open(folderpath+"/concepts/"+first+".con").read().split("\n")
        foldername = folderpath.replace("/", "**")
        f = open("correct/"+foldername+"**"+file, "w")
        parsei2b2Annotations(f, text, "ast", ast)
        parsei2b2Annotations(f, text, "con", conc)
        f.close()


## print the word count for each annotation as a frequnecy dictionary
def annotationSizeTable():
    punc = [x for x in string.punctuation]
    regex = re.compile('[\s' + "|\\".join(punc)+ ']+')
    annotations = {}
    all = []
    data = open("n2c2.json").read().split("\n")

    for dat in data:
        if dat == "":
            continue
        df = json.loads(dat)
        text = df["text"]
        correct = df["mentions"]

        for corr in correct:
            start = int(corr["start_offset"])
            end =  int(corr["end_offset"])
            substring = text[start:end]
            prev = 0
            count = 0
            ranges = []
            for match in regex.finditer(substring.lower()):
                ranges.append((prev, match.start()))
                prev = match.end()
            ranges.append((prev, len(substring)))
            if len(ranges) not in annotations:
                annotations[len(ranges)] = 0
            all.append(len(ranges))
            annotations[len(ranges)] = annotations[len(ranges)] +1
    all = np.array(all)
    print("min", np.min(all))
    print("max", np.max(all))
    print("average", np.average(all))

def createN2C2Json():
    f = open("n2c2.json", "w")
    files = os.listdir("correct")
    for file in files:
        correct = open("correct/"+file).read().split("\n")
        folder = "/".join(file.split("**")[:-1])
        filename = file.split("**")[-1]
        correct = open("correct/"+file).read().split("\n")
        text = open(folder + "/txt/"+filename).read()
        res = {}
        res["_id"] = file
        res["text"] = text
        res["mentions"] = []
        for corr in correct:
            opt = corr.split("||")
            if len(opt) < 2:
                continue
            res["mentions"].append({"start_offset": int(opt[0]) , "end_offset": int(opt[1])})
        json.dump(res, f)
        f.write("\n")
    f.close()


### Stanza set up folder generation code ###


CLIENT = CoreNLPClient(timeout=100000, memory='16G', be_quiet=True)

# get noun phrases with tregex
def getTokensAndRanges(client, text, pattern="NP",startOffset=0):
    lines = text.split("\n")
    tokens = []
    for i in range(len(lines)):
        offset = 0
        if i > 0:
            offset = len("\n".join(lines[0:i]))+1
        matches = client.tregex(lines[i],pattern,annotators="tokenize,ssplit,pos,lemma,parse")
        for sentence in matches["sentences"]:
            for phrase in sentence:
                start = sentence[phrase]["characterOffsetBegin"] + offset
                end = sentence[phrase]["characterOffsetEnd"] + offset
                tokens.append({"startIndex":start+startOffset,"endIndex":end+startOffset})
    return tokens

def stanza(text, i):

    return tokens

def ngram(text, n):
    punc = [x for x in string.punctuation]
    regex = '('+"|".join(["(\s|^)"+stop+"(\s|$)" for stop in stopwords.words('english')])+'|\n|[\s' + "|\\".join(punc)+ ']{'+ str(1)+',})'
    prog = re.compile(regex)
    prev = 0
    count = 0
    ranges = []
    for match in prog.finditer(text.lower(),overlapped=True):
        if match.start()> prev:
            ranges.append({"startIndex": prev, "endIndex": match.start()})
        prev = match.end()
    if prev < len(text):
        ranges.append({"startIndex":prev, "endIndex":len(text)})
    ranges = [{"startIndex":0,"endIndex":0}]*(n-1) + ranges + [{"startIndex":len(text),"endIndex":len(text)}]*(n-1)

    res = []
    for i in range(len(ranges)-(n-1)):
        start = ranges[i]["startIndex"]
        end = ranges[i+(n-1)]["endIndex"]
        res.append({"startIndex":start, "endIndex":end})
    return res


def getTotalSpans(folders):
    count = 0
    if isinstance(folders,str):
        folders = [folders]
    for folder in folders:
        for i in range(13,180):
            try:
                lines = open(folder+"/record-"+str(i)+".txt").read().split("\n")
            except:
                continue
            count = count + len(lines)
    return count


def analyzeExperiments():
    print("Dataset & Method & Number of Tokens & Precison & Recall & F-1 \\\\")
    precisionAndRecall("n2c2")


def precisionAndRecall(name):
    folders = FOLDERS
    data = {}
    for folder in folders:
        filename=getFileName(folder)
        samples = open("experiments/"+name+filename+".txt").read().split("\n")
        nums = [float(x.split("||")[-1]) for x in samples[:-1]]
        tot = np.array([float(x.split("||")[-2]) for x in samples[:-1]])
        nums = np.array(nums)
        perf = nums[nums == 1.0]
        data[str(folder)] = {}
        data[str(folder)]["tp"] = len(perf)
        data[str(folder)]["tpfp"] = np.sum(tot)
        data[str(folder)]["tot"] = len(tot)
        data[str(folder)]["avgOvlp"] = np.average(tot)
    folders = list(data.keys())
    labels = ["1-gram", "2-gram", "3-gram", "Wildgram", "Wildgram + 1-gram", "Stanza", "1-gram - 3-gram"]

    for i in range(len(folders)):
        prec = data[folders[i]]["tp"]/data[folders[i]]["tpfp"]
        rec = data[folders[i]]["tp"]/data[folders[i]]["tot"]
        f1 = sp.hmean([prec, rec])
        print(name +" & " + labels[i] +" & "+str(round(data[folders[i]]["tpfp"])) +" & "+str(round(prec, 3)) + " & " + str(round(rec, 3)) + " & " + str(round(f1,3))+  "\\\\")



def getValsFunc(data, func, file):
    lines = 0
    res = ""
    if os.path.exists(file):
        lines = len(open(file).read().split("\n"))
        res = open(file).read()
    f = open(file, "w+")
    f.write(res)
    all_vals = []
    count = 0
    for mention in data:
        if not mention:
            continue
        mentiondict = json.loads(mention)
        print(file, mentiondict["_id"])
        text = mentiondict["text"]
        start = time.time()
        tokens = func(text)
        diff = time.time()-start
        all_vals.append(diff/(len(text)/100))
        for code in mentiondict["mentions"]:
            count = count + 1
            if count <= lines:
                continue
            tot, score = bestOverlap(code["start_offset"], code["end_offset"], tokens)
            f.write(mentiondict["_id"]+"||"+str(code["start_offset"])+"||"+str(code["end_offset"])+"||"+text[code["start_offset"]: code["end_offset"]]+"||"+str(tot)+"||"+str(score)+"\n")
    f.close()
    return np.average(np.array(all_vals))

def onegram(text):
    return ngram(text, 1)
def twogram(text):
    return ngram(text, 2)
def threegram(text):
    return ngram(text, 3)

def wildgram1gram(text):
    return wildgram(text, stopwords=stopwords.words('english'), topicwords=["\d+", "\d+\.\d+","will","reveal", "revealed", "h/o", "s/p","denies", "denied"], returnNoise=False, includeParent=False)
def basewildgram(text):
    return wildgram(text,stopwords=stopwords.words('english'), topicwords=["\d+", "\d+\.\d+","will","reveal", "revealed", "h/o", "s/p","denies", "denied"], include1gram=False, returnNoise=False,includeParent=False)

def onetwothreegram(text):
    one = ngram(text, 1)
    two = ngram(text, 2)
    three = ngram(text, 3)
    return one+two+three

def runStanzaOnJson(jsonfile):
    f = open(jsonfile + ".json")
    data = f.read().split("\n")
    if not os.path.exists('stanza'):
        os.makedirs('stanza')
    all_vals = []
    count = 0
    done = 0
    with CLIENT:
        for dat in data:
            count = count + 1
            print("Checking", count, "of", len(data))
            if dat == "":
                continue
            mentiondict = json.loads(dat)
            text =  mentiondict["text"]
            if os.path.exists('stanza/'+mentiondict["_id"]):
                stuff =  open("stanza/"+mentiondict["_id"]).read()
                if "<GKT>EOF<GKT>" in stuff:
                    continue
            f = open("stanza/"+mentiondict["_id"], "w")
            const = 15000
            for i in range(0, len(text), const):
                if i+const < len(text):
                    sub = text[i:i+const]
                else:
                    sub = text[i:]
                start = time.time()
                tokens = getTokensAndRanges(CLIENT,sub, "NP", i) + getTokensAndRanges(CLIENT,sub, "VP", i)
                diff = time.time()-start
                all_vals.append(diff/(len(sub)/100))
                for code in mentiondict["mentions"]:
                    if i < int(code["start_offset"]) and (i+const) > int(code["end_offset"]):
                        tot, score = bestOverlap(code["start_offset"], code["end_offset"], tokens)
                        f.write(mentiondict["_id"]+"||"+str(code["start_offset"])+"||"+str(code["end_offset"])+"||"+text[code["start_offset"]: code["end_offset"]]+"||"+str(tot)+"||"+str(score)+"\n")
            f.write("<GKT>EOF<GKT>")
            f.close()
            print("average speed per 100 characters so far:", np.average(all_vals))
    f = open("experiments/"+jsonfile+"stanza.txt", "w")
    for dat in data:
        lines = open("stanza/"+mentiondict["_id"]).read()
        lines = "\n".join(lines.split("\n")[:-1])
        f.write(lines)
    f.close()




def runExperimentOnJson(jsonfile):
    f = open(jsonfile + ".json")
    data = f.read().split("\n")
    functions = [(onegram, "ngram1"), (twogram, "ngram2"), (threegram, "ngram3"), (wildgram1gram, "wildgram1gram"),(basewildgram, "wildgram"), (onetwothreegram,"ngram[1, 2, 3]")]
    doctot = []
    for func in functions:
        print(jsonfile, func)
        file = "experiments/"+jsonfile+ func[1]+".txt"
        doctime = getValsFunc(data, func[0], file)
        doctot.append(jsonfile + " & " + func[1] + " & " + str(doctime))

    print("Dataset & Method & Average Speed per 100 characters \\\\")
    for doc in doctot:
        print(doc + "\\\\")



def bestOverlap(start, end, ranges):
    max = 0
    tot = 0
    corrset = set(range(int(start), int(end)))
    for option in ranges:
        punset =set(range(int(option["startIndex"]), int(option["endIndex"])))
        diff = len(corrset.intersection(punset))/len(corrset.union(punset))
        if diff > max:
            max = diff
        if diff > 0.0:
            tot = tot+1
    return tot, max

def getFileName(folders):
    if isinstance(folders, str):
        return folders
    filename=""
    for fold in folders:
        filename = fold + filename
    return filename

def getMetaData():
    files = os.listdir("correct")
    ast = 0
    con = 0
    for file in files:
        lines = open("correct/"+file).read().split("\n")
        for line in lines:
            if "ast" in line:
                ast = ast + 1
            if "con" in line:
                con = con + 1
    print("ast", ast)
    print("con", con)
    print("number of files", len(files))

print("Please move the i2b2-2010 folders into this directory before running anything.")
print("Please also make sure you have all the relevant packages downloaded, and have set up stanza to support the CoreNLPClient.")
print("For any errors you have feel free to reach out to me at turneg@uw.edu.")
print("Bear in mind, the pre-processing process for stanza can take a while. That's normal.")
y = input("Ready to go? Press any key to continue.")




# turns word count metrics into index metrics and combines ast, concept into one file per record
parseData("i2b2-2010-originaldata/test")
parseData("i2b2-2010-originaldata/training/beth")
parseData("i2b2-2010-originaldata/training/partners")
parseData("i2b2-2010-originaldata/training/upmcd")
getMetaData()
createN2C2Json()
annotationSizeTable() # prints out the annotation token counts used in table 1 in the paper.
runStanzaOnJson("n2c2")
runExperimentOnJson("n2c2") #for each method, compare documents to base truth, and stick the results into a file in experiments folder#
analyzeExperiments() # plot precision and recall as a table, n2c2
